(* ::Package:: *)

(* ::Title:: *)
(*FourierPlus*)


(* ::Text:: *)
(*Rhys G. Povey <rhyspovey@gmail.com>*)


(* ::Section:: *)
(*Front End*)


BeginPackage["FourierPlus`"];


(* ::Subsection:: *)
(*Fourier zooming*)


FourierZoom::usage="FourierZoom[original array,position,zoom amount] samples the DTFT near position pos with respect to the DFT by a factor given by zoom. The zoomed indices cover the region [pos-\[CapitalDelta],pos+\[CapitalDelta]] with \[CapitalDelta] = (n-1)/(2 zoom). The interpolated original position value = pos - \[CapitalDelta] + \!\(\*SubscriptBox[\(position\), \(zoomed\)]\)/zoom. With this notation frequency = position - 1.";


FourierZoom::array="Input data is not a rectangular array.";


FourierZoom::range="Number of ranges given should match dimension.";


FourierZoom::output="Output type `1` not recognized.";


UnzoomedPosition::usage="UnzoomedPosition[zoomed position, zoom center, zoom amount, dimensions] returns the position in the original array corresponding to the zoomed position. Can be used as operator UnzoomedPosition[zoom center, zoom amount, dimensions][zoomed position].";


ZoomedPosition::usage="ZoomedPosition[original position, zoom center, zoom, dimensions] returns the zoomed position corresponding to the given original array position. Can be used as operator ZoomedPosition[zoom center, zoom, dimensions][original position].";


FindZoomedPeaks::usage="FindZoomedPeaks[zoomed array, zoom center, zoom amount, number] returns the original index location of number of the largest peaks in given zoomed array. The input zoomed array should be real numbers. After finding each peak a 'pixel' is cutout using the given zoom parameters. Zoomed array should be real numbers.";


SigmaZoomedPeak::usage="SigmaZoomedPeak::usage[peak position, zoomed array, zoome center, zoom amount] fits a symmetric Gaussian and returns \[Sigma]. The input zoomed array needs to be real numbers, i.e. \!\(\*SuperscriptBox[\(Abs\), \(2\)]\).";


FourierFindFrequencies::usage="FourierFindFrequencies[original array,number] finds a number of frequencies from peaks in fourier space. Has option \"PixelResolve\" to find peak location at sub-pixel resolution.";


(* ::Subsection:: *)
(*Periods and lattice vectors*)


CombineMirroredVectors::usage="CombineMirroredVectors[vectors] reduces a set of vectors in half by mirroring and averaging corresponding vectors together.";


CombineMirroredVectors::even="An even number of vectors is required.";


PairMirroredVectors::usage="PairMirroredVectors[vectors] reteurns a set of half the length of vectors with pairs of mirrored vectors.";


PairMirroredVectors::even="An even number of vectors is required.";


CombineOverlapVectors::usage="CombineMirroredVectors[vectors,overlap] averages overlap number of closest vectors together.";


CombineOverlapVectors::number="Number of vectors must be divisible by overlap number.";


FourierReciprocalVectors::usage="FourierReciprocalVectors[vectors,dimensions] returns the reciprocal (or vice-versa) lattice vectors for an array with given dimensions. The number of vectors given should equal the number of dimensions.";


FourierPositionFrequency::usage="FourierPositionFrequency[position,dimensions] returns the frequency vector corresponding to position in Fourier array. Can be used as operator FourierPositionFrequency[dimensions][position].";


ReciprocalFrequencies::usage="ReciprocalFrequencies[vectors] gives a set of reciprocals (periods) to a full set (including negative) frequencies.";


(* ::Subsection::Closed:: *)
(*Translating*)


ModData::usage="ModData[data,period,offset] takes data of the form {{\!\(\*SubscriptBox[\(x\), \(\(1\)\(,\)\)]\)\!\(\*SubscriptBox[\(y\), \(1\)]\),\[Ellipsis],\!\(\*SubscriptBox[\(f\), \(1\)]\)},{\!\(\*SubscriptBox[\(x\), \(\(2\)\(,\)\)]\)\!\(\*SubscriptBox[\(y\), \(2\)]\),\[Ellipsis],\!\(\*SubscriptBox[\(f\), \(2\)]\)},\[Ellipsis]} and applies Mod to the coordinates {\!\(\*SubscriptBox[\(x\), \(i\)]\),\!\(\*SubscriptBox[\(y\), \(i\)]\),\[Ellipsis]}.";


CycleArray::usage="CycleArray[array,{\!\(\*SubscriptBox[\(s\), \(1\)]\),\!\(\*SubscriptBox[\(s\), \(2\)]\),\[Ellipsis]}] cycles array by shift \!\(\*SubscriptBox[\(s\), \(1\)]\) in the first level, \!\(\*SubscriptBox[\(s\), \(2\)]\) in the second level and so on.";


(* ::Subsection:: *)
(*Graphics*)


GraphicsCoord::usage="GraphicsCoord[array index, array dimensions] maps an array index to its Graphics coordinate when displayed in ArrayPlot. Can be used as operator GraphicsCoord[dimensions][array index]. Note that array dimensions = Reverse[image dimensions].";


ArrayIndex::usage="ArrayIndex[graphics coord, image dimensions] maps a graphics coordinate to its (interpolated) Array index when formatted with ImageData, needs rounding to be used as an actual index. Can be used as operator ArrayIndex[dimensions][graphics coord]. Note that image dimensions = Reverse[array dimensions].";


GraphicsVector::usage="GraphicsVector[array vector] converts an array vectors {rows, columns} to a graphics vector {columns,-rows}.";


ArrayVector::usage="ArrayVector[graphic vector] converts a graphic vector {x, y} to an array vector {-y,x}";


ArrayPlotZoomHighlight::usage="ArrayPlotZoomHighlight[array, zoom center, zoom amount, translate] gives an ArrayPlot of array (translated) showing the region that will be zoomed. Has option \"ZoomStyle\".";


ArrayPlotZoomCutout::usage="ArrayPlotZoomCutout[array, zoom center, zoom amount] gives an ArrayPlot of array where the zooming will cover.";


ArrayPlotZoomedPeaks::usage="ArrayPlotZoomedPeaks[zoomed array, zoom center, zoom amount, original peaks indices] gives an ArrayPlot of zoomed data showing list of peaks (given by original indices). Shows pixel cutout region around each peak. Has option \"ZoomStyle\".";


(* ::Subsection:: *)
(*Defunct*)


ReciprocalCombinations::usage="ReciprocalCombinations[vectors] gives all combinations of Inverse[subset\[Transpose]] for subsets of vectors.";


ReciprocalCombinations::lengths="Input vector lengths not consistent.";


ReciprocalCombinations::defunct="ReciprocalCombinations does not always produce the desired result. Try ReciprocalFrequencies on the full set (including negative) of frequencies.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


(* ::Subsection:: *)
(*Fourier zooming*)


FourierZoom[array_List,position_,zoom_?NumberQ,options:OptionsPattern[]]:=Module[
{d,n,a,b,poslist,\[CapitalDelta],func,posmap,fourierzoom,ones,fourieroneszoom,output,rescale,zoomdata,zoomones},
d=Depth[array]-1; (* dimension *)
n=Dimensions[array]; (* number of points in each dimension *)
If[Length[n]=!=d,Message[FourierZoom::array]];
poslist=Flatten[{position}]; (* 1 dim support *)
\[CapitalDelta]=(n-1)/(2 zoom);

{a,b}=OptionValue[FourierParameters];
func=Product[Exp[(2\[Pi] I b)/n[[\[FormalI]]] ({##}[[\[FormalI]]]-1)(poslist[[\[FormalI]]]-1-(n[[\[FormalI]]]-1)/(2 zoom))],{\[FormalI],d}]&;
posmap=N[(position-\[CapitalDelta]+#/zoom)]&;
If[OptionValue[Verbose],Print["Zooming data"]];
zoomdata=array Array[func,n];
If[OptionValue[Verbose],Print["Performing zoomed FFT"]];
fourierzoom=Fourier[zoomdata,FourierParameters->{a,b/zoom}];

(* Remove periodic artifacts *)
output=If[OptionValue["ArtifactRemoval"],
If[OptionValue[Verbose],Print["Removing zoom periodicity artifacts"]];
ones=Array[1&,n];
fourieroneszoom=If[OptionValue["SaveArtifact"],
FourierZoomArtifact[position,zoom,n],
FourierZoom[ones,position,zoom,"ArtifactRemoval"->False,Verbose->False]
];
rescale=Re[Fourier[array][[Sequence@@ConstantArray[1,d]]]]/Re[Fourier[ones][[Sequence@@ConstantArray[1,d]]]];
If[OptionValue[Verbose],Print[StringTemplate["Rescaling artifacts by ``"]@rescale]];
fourierzoom-rescale fourieroneszoom,
fourierzoom
];

(* output *)
Switch[OptionValue["Output"],
Array,output,
Association,KeyMap[posmap,Association@@Drop[ArrayRules@SparseArray[output],-1]],
_,Message[FourierZoom::output,OptionValue["Output"]];output
]
];


Options[FourierZoom]={FourierParameters->OptionValue[Fourier,FourierParameters],Verbose->False,"Output"->Array,"ArtifactRemoval"->True,"SaveArtifact"->True};


UnzoomedPosition[newposition_,zoomcenter_,zoom_,dimensions_]:=N[zoomcenter-(dimensions-1)/(2 zoom)+newposition/zoom];


UnzoomedPosition[zoomcenter_,zoom_,dimensions_][newposition_]:=N[zoomcenter-(dimensions-1)/(2 zoom)+newposition/zoom];


ZoomedPosition[oldposition_,zoomcenter_,zoom_,dimensions_]:=zoom(oldposition-zoomcenter+(dimensions-1)/(2 zoom));


ZoomedPosition[zoomcenter_,zoom_,dimensions_][oldposition_]:=zoom(oldposition-zoomcenter+(dimensions-1)/(2 zoom));


FindZoomedPeaks[zoomedpowerarray_,zoomcenter_,zoom_,number_Integer,options:OptionsPattern[]]:=Module[
{d,n,remaining,reap,peakloc,unzoomedpeak,cutmin,cutmax},
d=Depth[zoomedpowerarray]-1;
n=Dimensions[zoomedpowerarray];
(* list of index \[Rule] value rules *)
remaining=Normal[If[Head[zoomedpowerarray]=!=Association,Association@@ArrayRules[zoomedpowerarray],zoomedpowerarray]];

reap=Reap[
Do[
peakloc=First@Keys[MaximalBy[remaining,Values]];
unzoomedpeak=UnzoomedPosition[zoomcenter,zoom,n][peakloc];
If[OptionValue[Verbose],Print[StringTemplate["Peak `` found at original index ``"][\[FormalI],unzoomedpeak]]];
Sow[unzoomedpeak];
cutmin=Floor@ZoomedPosition[zoomcenter,zoom,n][unzoomedpeak-ConstantArray[1,d]];
cutmax=Ceiling@ZoomedPosition[zoomcenter,zoom,n][unzoomedpeak+ConstantArray[1,d]];
remaining=Select[remaining,Not[And@@Table[cutmin[[\[FormalI]]]<Keys[#][[\[FormalI]]]<cutmax[[\[FormalI]]],{\[FormalI],d}]]&];
,{\[FormalI],number}]
];

(* output *)
reap[[2,1]]

];


Options[FindZoomedPeaks]={Verbose->False,"ReturnSigma"->False};


SigmaZoomedPeak[peakposition_List,zoomedpowerarray_,zoomcenter_,zoom_,options:OptionsPattern[]]:=Module[
{n,d,cutmin,cutmax,peakdata,peakarray,fit,A,\[Sigma],\[Mu],x},
n=Dimensions[zoomedpowerarray];
d=Depth[zoomedpowerarray]-1;

cutmin=Inner[Max,ConstantArray[1,d],Floor[ZoomedPosition[zoomcenter,zoom,n][peakposition-1]],List];
cutmax=Inner[Min,n,Ceiling[ZoomedPosition[zoomcenter,zoom,n][peakposition+1]],List];

(*peakarraypwr=Abs[zoomedarray[[Sequence@@((#[[1]];;#[[2]])&/@Join[{cutmin}\[Transpose],{cutmax}\[Transpose],2])]]]^2;*)
peakarray=zoomedpowerarray[[Sequence@@((#[[1]];;#[[2]])&/@Join[{cutmin}\[Transpose],{cutmax}\[Transpose],2])]];
peakdata=Join[Keys[#],{Values[#]}]&/@Drop[ArrayRules[peakarray],-1];
If[OptionValue[Verbose],Print[If[d==1,
	ListPlot[peakarray,PlotLabel->peakposition],
	ArrayPlot[peakarray,PlotLabel->peakposition]
]]];

fit=NonlinearModelFit[
peakdata,
{Evaluate[A Simplify@Product[PDF[NormalDistribution[\[Mu][\[FormalI]],\[Sigma]],x[\[FormalI]]],{\[FormalI],d}]],\[Sigma]>0},
Evaluate[{
{A,peakarray[[Sequence@@(Floor[Dimensions[peakarray]/2])]]},{\[Sigma],Floor[Mean@Dimensions[peakarray]/4]}
}~Join~Table[{\[Mu][\[FormalI]],Floor[Dimensions[peakarray][[\[FormalI]]]/2]},{\[FormalI],d}]
]
,Evaluate[Table[x[\[FormalI]],{\[FormalI],d}]]];

(* diagnostic *)
If[OptionValue[Verbose],Print[
Switch[d,
	2,Show[{
		ListPlot3D[peakdata,PlotStyle->Directive[RGBColor[0.37254901960784315`, 0.6196078431372549, 0.6274509803921569],Opacity[0.5]],PlotLegends->{"Fourier data"}],
		Plot3D[fit[\[FormalR],\[FormalC]],{\[FormalR],1,Dimensions[peakarray][[1]]},{\[FormalC],1,Dimensions[peakarray][[2]]},PlotStyle->Directive[Opacity[0.5],RGBColor[0.829997, 0.099994, 0.119999]],PlotLegends->{"Fit"}]
	}],
	1,Show[{
		ListPlot[peakdata,PlotStyle->Directive[RGBColor[0.37254901960784315`, 0.6196078431372549, 0.6274509803921569]],PlotLegends->{"Fourier data"}],
		Plot[fit[\[FormalR]],{\[FormalR],1,Dimensions[peakarray][[1]]},PlotStyle->Directive[Opacity[0.5],RGBColor[0.829997, 0.099994, 0.119999]],PlotLegends->{"Fit"}]
	}]
]]];

(* output *)
(\[Sigma]/.fit["BestFitParameters"])/zoom
];


Options[SigmaZoomedPeak]={Verbose->False};


PixelZoomedPeak[array_,peakposition_,options:OptionsPattern[]]:=Module[
{n,zoom,zoomarray,zoompeak,peak,\[Sigma]},
n=Dimensions[array];
zoom=Floor[Min[n]/2];
zoomarray=FourierZoom[array,peakposition,zoom,FilterRules[{options},Options[FourierZoom]]];
zoompeak=First@Keys[MaximalBy[ArrayRules[Abs[zoomarray]^2],Values]];
peak=UnzoomedPosition[peakposition,zoom,n][zoompeak];
If[OptionValue[Verbose],Print[StringTemplate["Pixel resolved peak found at ``"]@peak]];
If[OptionValue["ReturnSigma"],
	If[OptionValue[Verbose],Print["Fitting peak \[Sigma]"]];
	\[Sigma]=SigmaZoomedPeak[peak,Abs[zoomarray]^2,peakposition,zoom,FilterRules[{options},Options[SigmaZoomedPeak]]]
];
(* output *)
If[OptionValue["ReturnSigma"],
Around[#,\[Sigma]]&/@peak,
peak]
];


Options[PixelZoomedPeak]={"ArtifactRemoval"->True,Verbose->False,"SaveArtifact"->True,"ReturnSigma"->False};


FourierFindFrequencies[array_,number_,options:OptionsPattern[]]:=Module[
{d,n,zoom1,zoom1array,peaks1,peaks,fourierzoomoptions},
d=Depth[array]-1;
n=Dimensions[array];
zoom1=OptionValue["InitialZoom"];
fourierzoomoptions=Join[DeleteCases[FilterRules[{options},Options[FourierZoom]],"ArtifactRemoval"->_],{"ArtifactRemoval"->OptionValue["ArtifactRemoval"]}];

If[OptionValue[Verbose],Print[StringTemplate["Performing initial zoom of ``"]@zoom1]];
zoom1array=FourierZoom[array,ConstantArray[1,d],zoom1,fourierzoomoptions];
peaks1=FindZoomedPeaks[Abs[zoom1array]^2,ConstantArray[1,d],zoom1,number,FilterRules[{options},Options[FindZoomedPeaks]]];
If[OptionValue[Verbose]\[And](d==2),Print[ArrayPlotZoomedPeaks[Log[Abs[zoom1array]^2],{1,1},zoom1,peaks1]]];

If[OptionValue[Verbose]\[And]OptionValue["PixelResolve"],Print["Performing pixel resolution peak finding"]];
peaks=If[OptionValue["PixelResolve"],
PixelZoomedPeak[array,#,FilterRules[{options},Options[PixelZoomedPeak]]]&/@peaks1,
If[OptionValue["ReturnSigma"],
With[{peak=#},Around[#,SigmaZoomedPeak[peak,Abs[zoom1array]^2,ConstantArray[1,d],zoom1,FilterRules[{options},Options[SigmaZoomedPeak]]]]&/@peak]&/@peaks1,
peaks1]
];

(* output *)
FourierPositionFrequency[n]/@peaks
];


Options[FourierFindFrequencies]={"InitialZoom"->2,Verbose->False,"ArtifactRemoval"->True,"PixelResolve"->False,"ReturnSigma"->False,"SaveArtifact"->True};


(* ::Subsection:: *)
(*Periods and lattice vectors*)


AroundListQ[\[FormalX]_List]:=(Union[Head/@Flatten@\[FormalX]]==={Around});


CombineMirroredVectors[vectors_List]:=Module[
{remaining,x,p,objfunc},
objfunc=If[AroundListQ[vectors],
Norm[Through[#1["Value"]]+Through[#2["Value"]]]&,
Norm[#1+#2]&
];

remaining=vectors;
Reap[
While[Length[remaining]>0,
x=First@remaining;
p=First@TakeSmallestBy[Rest@remaining->"Index",objfunc[#,x]&,1]+1;
Sow[Mean[{x,-remaining[[p]]}]];
remaining=Delete[remaining,{{1},{p}}];
]
][[2,1]]
]/;If[EvenQ@Length[vectors],True,Message[CombineMirroredVectors::even];False];


PairMirroredVectors[vectors_List]:=Module[
{remaining,x,p,objfunc},
objfunc=If[AroundListQ[vectors],
Norm[Through[#1["Value"]]+Through[#2["Value"]]]&,
Norm[#1+#2]&
];

remaining=vectors;
Reap[
While[Length[remaining]>0,
x=First@remaining;
p=First@TakeSmallestBy[Rest@remaining->"Index",objfunc[#,x]&,1]+1;
Sow[{x,remaining[[p]]}];
remaining=Delete[remaining,{{1},{p}}];
]
][[2,1]]
]/;If[EvenQ@Length[vectors],True,Message[PairMirroredVectors::even];False];


CombineOverlapVectors[vectors_List,overlap_Integer]:=Module[
{remaining,x,pset,objfunc},
objfunc=If[AroundListQ[vectors],
Norm[Through[#1["Value"]]-Through[#2["Value"]]]&,
Norm[#1-#2]&
];

remaining=vectors;
Reap[
While[Length[remaining]>0,
x=First@remaining;
pset=Prepend[TakeSmallestBy[Rest@remaining->"Index",objfunc[#,x]&,overlap-1]+1,1];
Sow[Mean[remaining[[pset]]]];
remaining=Delete[remaining,{pset}\[Transpose]];
]
][[2,1]]
]/;If[Divisible[Length[vectors],overlap],True,Message[CombineOverlapVectors::number];False]


FourierReciprocalVectors[vectors_List,dimensions_List]:=Inverse[vectors\[Transpose]] . DiagonalMatrix[dimensions]


FourierPositionFrequency[position_,dimensions_]:=(position-1)/dimensions;


FourierPositionFrequency[dimensions_][position_]:=(position-1)/dimensions;


ReciprocalFrequencies[vectors_List]:=Module[{tuples,inverses},
tuples=Union[Flatten[Subsets[#,{2}]&/@Tuples[PairMirroredVectors[vectors]],1]];
inverses=Flatten[Inverse[#\[Transpose]]&/@tuples,1];
CombineOverlapVectors[inverses,Length[inverses]/Length[vectors]]
]


(* ::Subsection::Closed:: *)
(*Saving artifacts*)


FourierZoomArtifact[zoomcenter_,zoom_,\[FormalN]_]:=FourierZoomArtifact[zoomcenter,zoom,\[FormalN]]=FourierZoom[Array[1&,\[FormalN]],zoomcenter,zoom,"ArtifactRemoval"->False];


(* ::Subsection::Closed:: *)
(*Translating periodic data or array*)


ModData[data_List,period_,offset_:0]:=(Join[Mod[Drop[#,-1],period,offset],{Last@#}]&/@data)/;And[Depth[data]===3,Dimensions[data][[2]]===Length[period]]


CycleArrayLevel[array_List,amount_Integer,level_Integer]:=Module[{d},
d=Dimensions[array][[level]];
Join[
array[[Sequence@@ConstantArray[All,level-1]~Join~{1+Mod[amount,d];;}]],
array[[Sequence@@ConstantArray[All,level-1]~Join~{;;Mod[amount,d]}]]
,level]
];


CycleArray[array_List,amounts_List]:=Fold[CycleArrayLevel[#1,Sequence@@#2]&,array,Join[{amounts}\[Transpose],{Range@Length@amounts}\[Transpose],2]];


(* ::Subsection:: *)
(*Graphics and ArrayPlot for 2D*)


GraphicsCoord[arraycoord_List/;Length[arraycoord]===2,arraydimensions_List/;Length[arraydimensions]===2]:={arraycoord[[2]]-1/2,arraydimensions[[1]]-arraycoord[[1]]+1/2};


GraphicsCoord[arraydimensions_List/;Length[arraydimensions]===2][arraycoord_List/;Length[arraycoord]===2]:={arraycoord[[2]]-1/2,arraydimensions[[1]]-arraycoord[[1]]+1/2};


ArrayIndex[graphicscoord_List/;Length[graphicscoord]===2,imagedimensions_List/;Length[imagedimensions]===2]:=
{imagedimensions[[2]]-graphicscoord[[2]]+1/2,graphicscoord[[1]]+1/2};


ArrayIndex[imagedimensions_List/;Length[imagedimensions]===2][graphicscoord_List/;Length[graphicscoord]===2]:=
{imagedimensions[[2]]-graphicscoord[[2]]+1/2,graphicscoord[[1]]+1/2};


GraphicsVector[arrayvector_List/;Length[arrayvector]===2]:={arrayvector[[2]],-arrayvector[[1]]};


ArrayVector[gfxvector_List/;Length[gfxvector]===2]:={-gfxvector[[2]],gfxvector[[1]]};


ZoomRectangle[position_List,zoom_?NumberQ,dimensions_List]:=Module[{\[CapitalDelta]},
\[CapitalDelta]=(dimensions-1)/(2 zoom);
Thread[GraphicsCoord[dimensions]@Rectangle[position-\[CapitalDelta]-{1/2,1/2},position+\[CapitalDelta]+{1/2,1/2}],Rectangle]
];


ArrayPlotZoomHighlight[array_List,zoomposition_List,zoom_?NumberQ,cycletranslate_List:{0,0},options:OptionsPattern[]]:=Show[{
ArrayPlot[CycleArray[array,-cycletranslate],FilterRules[{options},Options@ArrayPlot]],
Graphics[{
Opacity[0],EdgeForm[OptionValue["ZoomStyle"]],ZoomRectangle[zoomposition+cycletranslate,zoom,Dimensions[array]]
}]
}];


Options[ArrayPlotZoomHighlight]=Options[ArrayPlot]~Join~{"ZoomStyle"->Red};


ArrayPlotZoomCutout[array_List,zoomposition_List,zoom_?NumberQ,options:OptionsPattern[]]:=Module[{dims,min,max,cycletranslate,arrayplotoptions},
dims=Dimensions[array];
min=Floor[UnzoomedPosition[zoomposition,zoom,dims]@{1,1}];
max=Ceiling[UnzoomedPosition[zoomposition,zoom,dims]@dims];
cycletranslate={0,0};
If[min[[1]]<1,cycletranslate+={1-min[[1]],0}];
If[min[[2]]<1,cycletranslate+={0,1-min[[2]]}];
If[max[[1]]>dims[[1]],cycletranslate+={dims[[1]]-max[[1]],0}];
If[max[[2]]>dims[[2]],cycletranslate+={0,dims[[2]]-max[[2]]}];

arrayplotoptions=Join[DeleteCases[FilterRules[{options},Options[ArrayPlot]],FrameStyle|PlotRangePadding->_],{FrameStyle->OptionValue[FrameStyle],PlotRangePadding->OptionValue[PlotRangePadding]}];
ArrayPlot[
Take[CycleArray[array,-cycletranslate],
Sequence@@Join[{min+cycletranslate}\[Transpose],{max+cycletranslate}\[Transpose],2]
],arrayplotoptions
]

];


Options[ArrayPlotZoomCutout]=Join[DeleteCases[Options[ArrayPlot],FrameStyle|PlotRangePadding->_],{FrameStyle->Red,PlotRangePadding->None}];


ArrayPlotZoomedPeaks[zoomedarray_,zoomcenter_,zoom_,peaksinput_,options:OptionsPattern[]]:=Module[
{dims,peaks,cent,min,max},
dims=Dimensions[zoomedarray];
peaks=If[IntegerQ[peaksinput],
FindZoomedPeaks[zoomedarray,zoomcenter,zoom,peaksinput,FilterRules[{options},Options[FindZoomedPeaks]]],
peaksinput
];

cent[\[FormalP]_List]:=ZoomedPosition[zoomcenter,zoom,dims][\[FormalP]];
min[\[FormalP]_List]:=Floor@ZoomedPosition[zoomcenter,zoom,dims][\[FormalP]-{1,1}];
max[\[FormalP]_List]:=Ceiling@ZoomedPosition[zoomcenter,zoom,dims][\[FormalP]+{1,1}];

Show[{
ArrayPlot[zoomedarray,FilterRules[{options},Options[ArrayPlot]]],
Graphics[Join@Table[{
Opacity[1],OptionValue["ZoomStyle"],Point[GraphicsCoord[dims][cent[\[FormalP]]]],
Opacity[0],EdgeForm[OptionValue["ZoomStyle"]],Rectangle[GraphicsCoord[dims][min[\[FormalP]]],GraphicsCoord[dims][max[\[FormalP]]]]
},{\[FormalP],peaks}]]
}
]
];


Options[ArrayPlotZoomedPeaks]=Options[ArrayPlot]~Join~{Verbose->False,"ZoomStyle"->Red};


(* ::Subsection:: *)
(*Defunct*)


ReciprocalCombinations[vectors_List]:=Module[
{tuples},
Message[ReciprocalCombinations::defunct];
tuples=Subsets[vectors,Union[Length/@vectors]];
Inverse[#\[Transpose]]&/@tuples
]/;If[Length[Union[Length/@vectors]]===1,True,Message[ReciprocalCombinations::lengths];False];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
